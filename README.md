Introduction
============

Tastier Restaurant Front end SPA

Installation
------------

#### Download:

Download from BitBucket

```
git clone https://cristijora93@bitbucket.org/cristijora93/tastier_rf_vue.git
```

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

```

For detailed explanation on how things work, checkout the [guide](https://github.com/vuejs-templates/webpack#vue-webpack-boilerplate) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


Documentation
-------------
Depending on the component you are working on you should have the following links handy.
- Vue.js - [online documentation](https://github.com/vuejs/vue)
- Vue-Router - [online documentation](https://github.com/vuejs/vue-router)
- Vuex - [online documentation](https://github.com/vuejs/vuex)

Browser Support
---------------
- IE 9+
- Firefox 
- Chrome 
- Safari
- Opera