var path = require('path')
var cssLoaders = require('./css-loaders')
var projectRoot = path.resolve(__dirname, '../')
var webpack=require('webpack')

module.exports = {
  entry: {
    app: './src/main.js',
    vendor:'./src/vendor.js'
  },
  output: {
    path: path.resolve(__dirname, '../dist/static'),
    distPath: path.resolve(__dirname, '../dist'),
    publicPath: '/static/',
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.vue'],
    fallback: [path.join(__dirname, '../node_modules')],
    root:'./src',
    alias: {
      'src': path.resolve(__dirname, '../src')
    }
  },
  plugins:[
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery:'jquery'
    }),
    new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.js", Infinity),
  ],
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: projectRoot,
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        loader: 'json',
        exclude: /node_modules/
      },
      {
        test: /\.html$/,
        loader: 'vue-html'
      },
      {
        test: /\.(png|jpg|gif|woff2?|eot|ttf|svg)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: '[name].[ext]?[hash:7]'
        },
        exclude: /node_modules/
      },
      { test: require.resolve('jquery'), loader: 'expose?jQuery!expose?$' } //set $ and jQuery to window
    ]
  },
  vue: {
    loaders: cssLoaders()
  },
  noParse: ['node_modules','static'],
  cache: true
}
