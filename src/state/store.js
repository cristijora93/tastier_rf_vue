import Vue from 'vue'
import Vuex from 'vuex'
import header from './modules/header/index'
import restaurant from './modules/restaurant/index'
import auth from './modules/auth/index'

Vue.use(Vuex);

export default  new Vuex.Store({
  modules:{
    header,
    restaurant,
    auth
  }
})
