export const setHeaderText=({dispatch},text)=>{
  dispatch('SET_HEADER_TEXT', text)
};
export const setLowerHeaderActiveTab=({dispatch},tabNumber)=>{
  dispatch('SET_LOWER_HEADER_ACTIVE_TAB', tabNumber)
};

export const setHeaderComponent=({dispatch},part,name)=>{
  dispatch('SET_HEADER_COMPONENT',part, name)
};

export const setHeaderColumns=({dispatch},part,columnsNr)=>{
  dispatch('SET_HEADER_COLUMNS', part,columnsNr)
};
export const displayHeaderPart=({dispatch},part,show)=>{
  dispatch('DISPLAY_HEADER_PART',part,show)
};


export const setLowerHeaderHeight = ({dispatch},value)=> {
  dispatch('SET_LOWER_HEADER_HEIGHT',value);

};

export const setUpperHeaderHeight = ({dispatch},value)=> {
  dispatch('SET_UPPER_HEADER_HEIGHT',value);

};

export const setWindowHeight = ({dispatch},value)=> {
  dispatch('SET_WINDOW_HEIGHT',value);

};
