import HeaderParts from '../../../helpers/constants/layout/HeaderParts'
export const mutations={
  SET_HEADER(state,header){
    state=header;
  },
  SET_HEADER_TEXT(state,text){
    state.text=text;
  },
  SET_LOWER_HEADER_ACTIVE_TAB(state,tabNumber){
    state.lower.activeTab=tabNumber;
  },
  SET_HEADER_COMPONENT(state,part,name){
    switch(part) {
      case HeaderParts.Left:
        state.leftSideComponent = name;
        break;
      case HeaderParts.Right:
        state.rightSideComponent = name;
        break;
    }
  },
  SET_HEADER_COLUMNS(state,part,columnsNr){
    switch(part){
      case HeaderParts.Left:
        state.leftColumns=columnsNr;
        break;
      case HeaderParts.Middle:
        state.middleColumns=columnsNr;
        break;
      case HeaderParts.Right:
        state.rightColumns=columnsNr;
        break;
    }
  },
  SET_LOWER_HEADER_HEIGHT(state,value){
   state.lower.height=value;
  },
  SET_UPPER_HEADER_HEIGHT(state,value){
    state.height=value;
  },
  SET_WINDOW_HEIGHT(state,value){
    state.windowHeight=value;
  },
  DISPLAY_HEADER_PART(state, part, visible){
    switch(part){
      case HeaderParts.Left:
        state.showLeftSide=visible;
        break;
      case HeaderParts.Right:
        state.showRightSide=visible;
        break;
      case HeaderParts.Lower:
        state.lower.visible=visible;
        break;
    }
  }
};
