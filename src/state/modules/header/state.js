export const state = {
  showLeftSide: false,
  showRightSide: false,
  text: '',
  leftColumns: 2,
  middleColumns: 8,
  rightColumns: 2,
  leftSideComponent: 'leftArrow',
  rightSideComponent: 'userInfo',
  backArrowFunction: ()=> {
    window.history.back()
  },
  windowHeight:0,
  height: 0,
  lower: {
    activeTab: 1,
    height: 0
  }
};
