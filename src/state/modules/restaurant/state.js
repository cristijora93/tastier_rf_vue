import config from '../../../services/config'
export const state = {
  restaurantId: config.settings.restaurantId,
  waiters: [],
  unallocatedUsers: [],
  waiter: {
    avatar: ""
  },
  tables: [],
  allTables: [],
  messages: [],
  errors: {
    addUsersError: '',
    deleteMessage: '',
    tableStatusError: '',
  },
  allocationMessage: '',
  isAllocationMessageVisible: false,
  currentTable: null,
  selectedUsers: [],
  loading: {
    table: false,
    unallocated: false,
    allocated: false,
    myTables: false,
    allTables: false,
    addUsers: false,
    deleteMessage: false,
    overlayClass: ''
  }
};
