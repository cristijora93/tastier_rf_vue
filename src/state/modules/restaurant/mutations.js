import LoadingParts from '../../../helpers/constants/layout/LoadingParts'
import ErrorTypes from '../../../helpers/constants/layout/ErrorTypes'
import store from '../../store'
export const mutations = {
  SET_TABLES(state, tables){
    state.tables = tables;
  },
  SET_ALL_TABLES(state, tables){
    state.allTables = tables;
  },
  UPDATE_OR_ADD_TABLE(state, table){
    var index = state.allTables.map(t=>t.id).indexOf(table.id);
    var route = store.state.route;
    if (index != -1) {
      if(table.order){
        if(table.order.isClosed){
          state.allTables.splice(index, 1);
          if (route.name == 'table') {
            state.currentTable = table;
          }
          return;
        }
      }
      state.allTables.splice(index, 1, table);
    }
    else {
      if(table.order) {
        if (table.order.isClosed) {
          if (route.name == 'table') {
            state.currentTable = table;
          }
        }
      }
      state.allTables.push(table);
    }
  },
  SET_CURRENT_TABLE(state, table){
    state.currentTable = table;
  },
  SET_WAITERS(state, waiters){
    state.waiters = waiters;
  },
  SET_WAITER(state, waiter){
    state.waiter = waiter;
  },
  SET_UNALLOCATED_USERS(state, users){
    state.unallocatedUsers = users;
  },
  SET_SELECTED_USERS(state, users){
    state.selectedUsers = users;
  },
  SET_MESSAGES(state, messages){
    state.messages = messages;
  },
  ADD_MESSAGE(state, message){
    state.messages.unshift(message);
  },
  SET_OVERLAY(state, value){
    if (value === true) {
      state.loading.overlayClass = 'overlay';
    }
    else {
      state.loading.overlayClass = '';
    }
  },
  SET_ALLOCATION_MESSAGE(state, value){
    state.allocationMessage = value;
  },
  DISPLAY_ALLOCATION_MESSAGE(state, value){
    state.isAllocationMessageVisible = value;
  },
  SET_ERROR(state, part, value){
    switch (part) {
      case ErrorTypes.AddUsersError:
        state.errors.addUsersError = value;
        break;
      case ErrorTypes.DeleteMessage:
        state.errors.deleteMessage = value;
        break;
      case ErrorTypes.TableStatusError:
        state.errors.tableStatusError = value;
        break;
      default:
        state.errors.addUsersError = value;
        break;
    }
  },
  SET_LOADING(state, part, value){
    switch (part) {
      case LoadingParts.Allocated:
        state.loading.allocated = value;
        break;
      case LoadingParts.Unallocated:
        state.loading.unallocated = value;
        break;
      case LoadingParts.MyTables:
        state.loading.myTables = value;
        break;
      case LoadingParts.AllTables:
        state.loading.allTables = value;
        break;
      case LoadingParts.Table:
        state.loading.table = value;
        break;
      case LoadingParts.AddUsers:
        state.loading.addUsers = value;
        break;
      case LoadingParts.DeleteMessage:
        state.loading.deleteMessage = value;
      default:
        state.loading.allTables = value;
        break;
    }
  },
  SET_LOADING_TABLE(state, value){
    var route = store.state.route;
    if (route.name == 'table') {
      state.loading.table = value;
    }
  },
  REMOVE_MESSAGE(state, messageId){
    var index = state.messages.map(x=>x.Message.Id).indexOf(messageId);
    if (index === -1) {
      window.console.log('Could not delete this message');
    }
    else {
      state.messages.splice(index, 1);
    }
  },
  REMOVE_MESSAGES(state, messagesIds){
    var msgIds = state.messages.map(x=>x.id);

    messagesIds.forEach(function (element) {
      var index = msgIds.indexOf(element.id);
      if (index === -1) {
        window.console.log('Could not delete this message');
      }
      else {
        state.messages.splice(index, 1);
      }
    });
  },
};
