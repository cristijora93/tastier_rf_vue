/**
 * Created by cristian.jora on 05.09.2016.
 */
import LoadingParts from '../../../../helpers/constants/layout/LoadingParts'
import ErrorTypes from '../../../../helpers/constants/layout/ErrorTypes'
import TableFilter from '../../../../helpers/constants/data/TableFilter'
import {PosService} from '../../../../services/service'
import store from '../../../store'

export const getTable = ({dispatch}, tableId, successCallBack, errorCallBack)=> {
  dispatch('SET_LOADING', LoadingParts.Table, true);
  PosService.getTable(tableId, TableFilter.Any, (table)=> {
    dispatch('SET_LOADING', LoadingParts.Table, false);
    dispatch('SET_CURRENT_TABLE', table);
    successCallBack(table);

  }, (error)=> {
    dispatch('SET_ERROR',ErrorTypes.TableStatusError,'Invalid link. Go back and try again.')
    errorCallBack();
  });
};

export const getTables = (dispatch)=> {
  dispatch('SET_LOADING', LoadingParts.AllTables, true);
  dispatch('SET_LOADING', LoadingParts.Allocated, true);
  dispatch('SET_LOADING', LoadingParts.Table, true);

  PosService.getAllTables((tables) => {
    dispatch('SET_ALL_TABLES', tables);
    dispatch('SET_LOADING', LoadingParts.AllTables, false);
    dispatch('SET_LOADING', LoadingParts.Allocated, false);
    dispatch('SET_LOADING', LoadingParts.Table, false);
  },()=>{
    dispatch('SET_LOADING', LoadingParts.AllTables, false);
    dispatch('SET_LOADING', LoadingParts.Allocated, false);
    dispatch('SET_LOADING', LoadingParts.Table, false);
  });
}
export const allocateUsersToTable = ({dispatch}, tableObj, successCallBack)=> {
  if (tableObj.CustomerIds.length == 0) {
    dispatch('SET_ERROR', ErrorTypes.AddUsersError, 'Nu user selected');
    return;
  }
  dispatch('SET_LOADING', LoadingParts.AddUsers, true);
  dispatch('SET_OVERLAY', true);

  PosService.addCustomerToTable(tableObj, ()=> {
    dispatch('SET_LOADING', LoadingParts.AddUsers, false);
    dispatch('SET_OVERLAY', false);
    successCallBack();
  }, (context, error, statusCode)=> {
    var errorMsg = '';
    switch (error.Code) {
      case 409:
        errorMsg = "Client was already allocated to a table.";
        break;
      case 100:
        errorMsg = "There is no such table.";
        break;
      case 106:
        errorMsg = "User is not currently checked in at a restaurant";
        break;
      default:
        errorMsg = "An error occurred. Please try again.";
        break;
    }
    switch (statusCode) {
      case 406:
        errorMsg = "Order operation in progress. Try again."
        break;
      case 409:
        errorMsg = "This table has several open orders, please allocate the user(s) to a different table or close all but 1 order on this table and try again.";
        break;
      default:
        if (errorMsg == '') {
          errorMsg = "An error occured. Please try again.";
        }
        break;
    }
    dispatch('SET_ERROR', ErrorTypes.AddUsersError, errorMsg);
    dispatch('SET_LOADING', LoadingParts.AddUsers, false);
    dispatch('SET_OVERLAY', false);
  })
};

export const deallocateCustomer = ({dispatch}, removeCustomerObj, successCallback, errorCallback)=> {
  PosService.deleteCustomerFromTable(removeCustomerObj, successCallback, errorCallback);
}

export const setCurrentTable = ({dispatch}, table)=> {
  dispatch('SET_CURRENT_TABLE', table);
};

export const setAllocationMessage = ({dispatch}, value)=> {
  dispatch('SET_ALLOCATION_MESSAGE', value);
};

export const displayAllocationMessage = ({dispatch}, value)=> {
  dispatch('DISPLAY_ALLOCATION_MESSAGE', value);
};

export const setLoadingTable = ({dispatch}, value)=> {
  dispatch('SET_LOADING_TABLE', value);
};

export const updateTable = (dispatch, table)=> {
  if (typeof table != 'object') return;

  dispatch('SET_LOADING', LoadingParts.Allocated, true);
  dispatch('SET_LOADING_TABLE', true);
  dispatch('SET_LOADING', LoadingParts.AllTables, true);
  dispatch('UPDATE_OR_ADD_TABLE', table);
  dispatch('SET_LOADING_TABLE', false);
  dispatch('SET_LOADING', LoadingParts.Allocated, false);
  dispatch('SET_LOADING', LoadingParts.AllTables, false);
}
