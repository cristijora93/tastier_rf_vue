/**
 * Created by cristian.jora on 05.09.2016.
 */
import LoadingParts from '../../../../helpers/constants/layout/LoadingParts'
import CustomerType from '../../../../helpers/constants/data/CustomerType'
import {StaffService,UserService} from '../../../../services/service'
export const getWaiters = ({dispatch}, restaurantId, errorCallback)=> {
  StaffService.GetStaffMembers(restaurantId, (responseData)=> {
    dispatch('SET_WAITERS', responseData)
  }, (error)=> {
    errorCallback(error);
  });

};

export const setWaiter = ({dispatch}, waiter)=> {
  dispatch('SET_WAITER', waiter)
};

export const setSelectedUsers = ({dispatch}, users)=> {
  dispatch('SET_SELECTED_USERS', users)
};

export const setError = ({dispatch}, part, value)=> {
  dispatch('SET_ERROR', part, value);
};

export const refreshUsers=(dispatch) =>{
  dispatch('SET_LOADING', LoadingParts.Unallocated, true);
  //dispatch('SET_LOADING', LoadingParts.Allocated, true);
  UserService.GetUsers(CustomerType.Unallocated, function (users) {
    dispatch('SET_UNALLOCATED_USERS', users);
    sessionStorage.setItem('users', JSON.stringify(users));
    dispatch('SET_LOADING', LoadingParts.Unallocated, false);
    //dispatch('SET_LOADING', LoadingParts.Allocated, false);
  });
}
