/**
 * Created by cristian.jora on 05.09.2016.
 */
import ErrorTypes from '../../../../helpers/constants/layout/ErrorTypes'
import {StaffService} from '../../../../services/service'
export const setMessages = ({dispatch}, messages)=> {
  dispatch('SET_MESSAGES', messages)
};

export const addMessage = ({dispatch}, message)=> {
  dispatch('ADD_MESSAGE', message)
};

export const removeMessage = ({dispatch}, messageId, successCallBack, errorCallBack)=> {
  var info = {MessageId: messageId};
  StaffService.removeMessage(info, ()=> {
    dispatch('REMOVE_MESSAGE', messageId);
    successCallBack();
  }, (context, reason, error, status)=> {
    dispatch('SET_ERROR', ErrorTypes.DeleteMessage, 'Could not delete the message');
    errorCallBack(context, reason, status);
  })

};

export const removeMessages = ({dispatch}, messageIds)=> {
  dispatch('REMOVE_MESSAGES', messageIds)
};
