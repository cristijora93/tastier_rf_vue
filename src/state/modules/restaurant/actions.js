import signalrObserver from '../../../helpers/event-utils/signalr-observer'
import CustomerType from '../../../helpers/constants/data/CustomerType'
import {UserService} from '../../../services/service'
import * as tableActions from './actions/table'
import * as messageActions from './actions/messages'
import * as userActions from './actions/users'

var initializeSignalREvents = ({dispatch})=> {
  //initialize data
  dispatch('SET_UNALLOCATED_USERS', []);
  dispatch('SET_TABLES', []);

  tableActions.getTables(dispatch);
  UserService.GetUsers(CustomerType.Unallocated, updateUsers);

  //subscribe to events
  //unallocated users
  var SignalrObserver = signalrObserver();
  SignalrObserver.onTableStatusChanged.subscribe(updateUsers);
  SignalrObserver.onMessageReceived.subscribe(updateUsers);
  //tables
  SignalrObserver.onTableStatusChanged.subscribe(refreshTable);
  SignalrObserver.onProfileChanged.subscribe(refreshTable);

  //messages
  SignalrObserver.getAllMessages.subscribe(function (messages) {
    dispatch('SET_MESSAGES', messages)
  });
  SignalrObserver.onMessageReceived.subscribe(function (message) {
    dispatch('ADD_MESSAGE', message)
  });

  SignalrObserver.forgetMany.subscribe(function (messagesIds) {
    dispatch('REMOVE_MESSAGES', messagesIds)
  });
  SignalrObserver.logout.subscribe(function () {
    sessionStorage.clear();
    location.href = location.origin; //redirect to homepage
  });

  function updateUsers() {
    userActions.refreshUsers(dispatch);
  }

  function refreshTable(table) {
    tableActions.updateTable(dispatch, table);
  }
};

module.exports = {
  getTable: tableActions.getTable,
  updateTable: tableActions.updateTable,
  allocateUsersToTable: tableActions.allocateUsersToTable,
  setCurrentTable: tableActions.setCurrentTable,
  setAllocationMessage: tableActions.setAllocationMessage,
  displayAllocationMessage: tableActions.displayAllocationMessage,
  deallocateCustomer: tableActions.deallocateCustomer,
  addMessage: messageActions.addMessage,
  removeMessage: messageActions.removeMessage,
  setMessages: messageActions.setMessages,
  removeMessages: messageActions.removeMessages,
  getWaiters: userActions.getWaiters,
  setWaiter: userActions.setWaiter,
  setSelectedUsers: userActions.setSelectedUsers,
  setError: userActions.setError,
  initializeSignalREvents,
}



