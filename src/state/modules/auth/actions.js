import {AuthenticationService,AuthScope,SessionContext} from '../../../services/service'
import {setWaiter} from './../restaurant/actions'
import store from './../../store'
export const setToken=({dispatch},token)=>{
  dispatch('SET_TOKEN', token)
}

export const setAuthenticated=({dispatch},value)=>{
  dispatch('SET_AUTHENTICATED', value)
}

export const authenticate = ({dispatch},data,successCallback,errorCallback)=> {
  store.dispatch('SET_OVERLAY', true);
  AuthenticationService.authenticate(data, function (response) {
    var waiter = {
      id: response.waiterId,
      firstName: response.waiterFirstName,
      lastName: response.waiterLastName,
      avatar: response.waiterAvatar
    };
    setWaiter(store, waiter);
    SessionContext.setClientId(response.clientId);
    if(response.accessToken){
      SessionContext.setSession(response);
      dispatch('SET_TOKEN',response.accessToken);
      sessionStorage.setItem('waiter', JSON.stringify(waiter));
      dispatch('SET_AUTHENTICATED',true);
      store.dispatch('SET_OVERLAY', false);
      successCallback();
    }
  }, function (error) {
    window.console.log(error);
    store.dispatch('SET_OVERLAY', false);
    errorCallback();

  });
}





