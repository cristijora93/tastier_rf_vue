export const mutations={
  TOGGLE_LOADING (state) {
    state.loading = !state.loading
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_AUTHENTICATED(state,value){
    state.authenticated=value;
  }
};
