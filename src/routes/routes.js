import Router from 'vue-router'
import Vue from 'vue'
import App from './../components/App.vue'
import LoginView from './../components/Login.vue'
import NotFoundView from './../components/404.vue'
import {initializeSignalREvents} from '../state/modules/restaurant/actions'
import store from '../state/store'
import {sync} from 'vuex-router-sync'

Vue.use(Router);
// Routing logic
var router = new Router({
  saveScrollPosition:true,
  history:true
});

function initializeRoutes() {
  // Routes
  router.map({
    '/': {
      component: function (resolve) {
        require(['./../components/waiter/waiterList.vue'], resolve);
      }
    },
    '/login/:id': {
      name: 'login',
      component: LoginView
    },
    '/users': {
      component: function (resolve) {
        require(['./../components/users/users.vue'], resolve);
      },
      auth: true,
      subRoutes: {
        '/unallocated': {
          component: function (resolve) {
            require(['./../components/users/unallocatedUsers.vue'], resolve);
          },
        },
        '/allocated': {
          component: function (resolve) {
            require(['./../components/users/allocatedUsers.vue'], resolve);
          }
        },

      }
    },
    '/add-users': {
      component: function (resolve) {
        require(['./../components/users/addUsersToTable.vue'], resolve);
      }
    },
    '/tables': {
      component: function (resolve) {
        require(['../components/table/list/table-list.vue'], resolve);
      },
      auth: true,
      subRoutes: {
        '/my': {
          component: function (resolve) {
            require(['../components/table/list/my-tables.vue'], resolve);
          },
        },
        '/all': {
          component: function (resolve) {
            require(['../components/table/list/all-tables.vue'], resolve);
          }
        }
      }
    },
    '/table/:id': {
      component: function (resolve) {
        require(['./../components/table/table-status.vue'], resolve);
      },

      auth: true,
      subRoutes: {
        '/details': {
          component: function (resolve) {
            require(['../components/table/table-details.vue'], resolve);
          },
          name: 'table',
        },
        '/add-users': {
          component: function (resolve) {
            require(['../components/table/add-users-to-table.vue'], resolve);
          },
          name:'table-add-users'
        }
      }
    },
    // not found handler
    '*': {
      component: NotFoundView
    }
  })

}

function initializeTokenVerification() {
  // Some middleware to help us ensure the user is authenticated.
  router.beforeEach(function (transition) {
    var store = transition.to.router.app.$store;
    var token = store.state.auth.token;
    var localStorageToken = sessionStorage.getItem('session');
    var isInvalidToken = token === null && !localStorageToken;
    if (transition.to.auth && isInvalidToken) {
      window.window.console.log('Not authenticated');
      transition.redirect('/')
    } else {
      initializeStateFromLocalStorage(transition.to.router.app.$store, transition);
      transition.next()
    }
  })
}

function initializeStateFromLocalStorage(store, transition) {
  var state = store.state;
  if (!state.restaurant.waiter || !state.restaurant.waiter.avatar) {
    let localStorageWaiter = JSON.parse(sessionStorage.getItem('waiter'));
    if (localStorageWaiter) {
      store.dispatch('SET_WAITER', localStorageWaiter);
    }
  }
  if (!state.auth.token) {
    let localStorageToken = sessionStorage.getItem('session');
    if (localStorageToken) {
      store.dispatch('SET_TOKEN', localStorageToken);
      store.dispatch('SET_AUTHENTICATED', true);
      initializeSignalREvents(store);
    }
  }

}

export function initializeRouter() {
  initializeRoutes();
  initializeTokenVerification();
  router.start(App, '#root'); //binds the app to the element with the id "root"
  sync(store, router); //makes a vuex module with the route data and keeps it in sync
}


