export default{
  computed:{
    tableUsers(){
      if(!this.currentTable){
        return [];
      }
      if(this.currentTable.users==null) return [];
      return this.currentTable.users;
    },
    tableUserCount(){
      if (this.currentTable) {
        if(this.currentTable.users){
          return this.currentTable.users.length;
        }
        
      }
      return 0;
    },
  }
}
