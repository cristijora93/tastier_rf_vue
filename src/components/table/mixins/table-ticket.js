import tableOrder from './table-order'
export default {
  mixins:[tableOrder],
  computed: {
    posOrderId(){
      if (this.order) {
        if (this.order.posOrderId == '') {
          return 'Open table in POS'
        }
        return this.order.posOrderId;
      }
      else {
        return 'Open table in POS'
      }
    },
  }
}
