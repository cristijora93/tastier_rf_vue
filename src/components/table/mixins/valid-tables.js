export default{
  computed: {
    tables(){
      if (!this.allTables) {
        return [];
      }
      return this.allTables.filter(t=> {
        if (!t.users) return false;
        return t.users.length > 0
      });
    }
  },
}
