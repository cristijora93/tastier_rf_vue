import tableOrder from './table-order'
export default{
  mixins:[tableOrder],
  computed:{
    formattedTableTime(){
      if(!this.tableTime || this.tableTime=='') return '';
      var date=this.tableTime;
      var hours = date.getHours();
      var minutes = date.getMinutes();
      if (hours < 10) hours = "0" + hours;
      if (minutes < 10) minutes = "0" + minutes;

      return hours + ':' + minutes;
    },
    tableTimeText(){
      return 'since '+this.formattedTableTime;
    },
    tableTime(){
      if (this.order) {
        return this.order.created;
      }
      return '';
    },
  }
}
