export default{
  computed:{
    order(){
      if (!this.currentTable) return null;
      return this.currentTable.order;
    }
  }
}
