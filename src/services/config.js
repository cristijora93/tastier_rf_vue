var settings = {
  restaurantId:'83BB19B3-33FE-4DB0-AF3D-C89D77BF57FE', //La Cote backend
  //restaurantId:'D5173CD4-286E-44CE-BD6B-2D9A6A3EE7EB', //La Cote IOS
  //restaurantId:'E2F603BB-DADA-4C92-B854-D50B62C7F4DF', //La Cote test
  debug: true,
  version:0.3,
  services: {
    apiPath: '/api/',

    authenticationService: {
      path: 'auth/',
      methods: {
        authenticate: '',
        refresh: 'refresh',
        information: 'information/'
      }
    },

    posService: {
      path: 'pos/',
      methods: {
        restaurant: 'restaurant/',
        table: 'table/',
        customer: 'customer/',
        order: 'order/'
      }
    },

    userService: {
      path: 'customer',
      methods: {
        crud: ''
      }
    },

    staffService: {
      path: 'staff/',
      methods: {
        crud: '',
        viewMessage:'message/viewed'
      },

    }
  },
};

var apiConfig = {
  endpoints: {
    local: 'https://localhost:44300',
    live: 'https://live.tastier.ch',
    development: 'https://dev.tastier.ch',
    staging: 'https://staging.tastier.ch',
    activeEndpoint: 'https://localhost:44301'
  }
};

export default{
  apiConfig: apiConfig,
  settings: settings,
}
