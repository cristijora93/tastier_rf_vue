var Dao = {
  parseCollection(data, handler) {
    var array = [];
    for (var i in data) {
      array.push(handler(data[i]));
    }
    return array;
  }
};

export default Dao;
