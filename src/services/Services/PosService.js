import config from '../config'
import AbstractServiceDao from './AbstractServiceDao'
import Dao from './Dao'
import OrderService from './OrderService'
import UserService from './UserService'
var PosService = {
  currentService: null,
  get service() {
    if (this.currentService == null) {
      return new AbstractServiceDao(config.settings.services.posService.path);
    }
    else return this.currentService;
  },
  getCustomers(successCallback, errorCallback) {
    this.service.retrieve(config.settings.services.posService.methods.customer, (responseData) => {
      successCallback(Dao.parseCollection(responseData, UserService.parseUser));
    }, (context, reason,status)=>{
      errorCallback(context, reason,status);
    });
  },
  getAllTables(successCallback, errorCallback) {
    this.service.retrieve(config.settings.services.posService.methods.table + 'all/', null, (responseData) => {
      successCallback(Dao.parseCollection(responseData, parseTable));
    }, (context, reason,status)=>{
      errorCallback(context, reason,status);
    });
  },
  getTable(tableId, filter, successCallback, errorCallback) {
    this.service.retrieve(config.settings.services.posService.methods.table, {
      tableId: tableId,
      filter: filter
    }, function (literal) {
      successCallback(parseTable(literal));
    }, ()=>{
      errorCallback();
    });
  },
  addCustomerToTable(jsonData, successCallback, errorCallback) {
    this.service.insertJSON(config.settings.services.posService.methods.customer + 'table/', jsonData, (responseData) => {
      successCallback(parseTable(responseData));
    }, (context, reason,status)=>{
      errorCallback(context, reason,status);
    });
  },
  deleteCustomerFromTable(jsonData, successCallback, errorCallback) {
    this.service.insertJSON(config.settings.services.posService.methods.customer + 'deallocate/', jsonData, () => {
      successCallback();
    },(context, reason,status)=>{
      errorCallback(context, reason,status);
    });
  },
};

function parseTable(data) {
  return {
    id: data.Id,
    name: data.Name,
    tableNumber: data.TableNumber,
    tableMessages: Dao.parseCollection(data.TableMessages, parseTableMessages),
    order: data.Order == null ? null : OrderService.parseOrder(data.Order),
    users: Dao.parseCollection(data.Users, parseTableAllocation)
  };
}
function parseTableMessages(data) {
  return {
    messageHeader: !/\S/.test(data.MessageHeader) ? null : data.MessageHeader,
    messages: data.Messages,
    type: data.Type,
    timestamp: new Date(data.Timestamp)
  };
}
function parseTableAllocation(data) {
  return {
    id: data.Id,
    tableId: data.TableId,
    allocated: data.Allocated,
    user: data.User == null ? null : UserService.parseUserLogin(data.User)
  };
}


export {PosService, parseTable};
