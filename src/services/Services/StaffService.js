import AbstractServiceDao from './AbstractServiceDao'
import Dao from './Dao'
import config from '../config'
var StaffDao = {
  currentService:null,
  get service(){
    if (this.currentService == null) {
      return new AbstractServiceDao(config.settings.services.staffService.path);
    }
    else return this.currentService;
  },
  GetStaffMembers(restaurantId, successCallback, errorCallback){
    this.service.retrieve(config.settings.services.staffService.methods.crud, {restaurantId: restaurantId}, function (responseData) {
      successCallback(Dao.parseCollection(responseData, StaffDao.parseStaff));
    }, ()=>{
      errorCallback();
    });
  },
  removeMessage(jsonData, successCallback, errorCallback){
    this.service.insertJSON(config.settings.services.staffService.methods.viewMessage, jsonData, () => {
      successCallback();
    }, (context,reason,status)=>{
      errorCallback(context,reason,status);
    });
  },
  parseStaff(data){
    return {
      id: data.Id,
      avatar: data.AvatarUrl,
      preName: data.PreName,
      lastName: data.LastName,
      loginName: data.LoginName
    };
  }
};

export default StaffDao;
