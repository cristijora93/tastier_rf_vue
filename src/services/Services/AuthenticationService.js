import config from '../config'
import {AuthScope,AuthenticationSuccess} from '../../helpers/constants/data/service-constants'
import AbstractServiceDao from './AbstractServiceDao'
var AuthenticationDao = {
  currentService:null,
  get service(){
    if (this.currentService == null) {
      return new AbstractServiceDao(config.settings.services.authenticationService.path);
    }
    else return this.currentService;
  },
  authenticate(requestData, successCallback, errorCallback){
    var oAuthRequest = {
      grant_type: 'password',
      username: requestData.userName,
      password: requestData.password,
      client_id: requestData.clientId,
      scope: AuthScope[requestData.scope]
    };
    this.service.insert(config.settings.services.authenticationService.methods.authenticate, oAuthRequest, (responseData)=> {
      successCallback(this.parseOAuthResponse(responseData));
    },(e, body)=> {
      errorCallback(this.parseOAuthError(body));
    });
  },
  getSessionInformation(successCallback, errorCallback) {
    var self=this;
    this.service.retrieve(config.settings.services.authenticationService.methods.information, null,
      function (responseData) {
      successCallback(self.parseSessionInformation(responseData));
    }, errorCallback);
  },
  parseOAuthResponse(data) {
    var expireDate = new Date();
    expireDate.setTime(expireDate.getTime() + (parseInt(data.expires_in) * 1000));
    return {
      refreshToken: data.refresh_token,
      expireDate: expireDate,
      accessToken: data.access_token,
      waiterId: data.Id,
      waiterFirstName: data.FirstName,
      waiterLastName: data.LastName,
      waiterAvatar: data.Avatar
    };
  },
  parseOAuthError(data) {
    return {
      status: AuthenticationSuccess[data.error],
      message: data.error_description
    };
  },
  parseSessionInformation(data) {
    return {
      userId: data.UserId,
      userRole: data.UserRole
    };
  }
};

export default AuthenticationDao;
