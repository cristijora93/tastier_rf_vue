import Dao from './Dao'
var OrderDao = {
  parseOrder(data) {
    return {
      closed: data.Closed != null ? new Date(data.Closed) : undefined,
      created: new Date(data.Created),
      posOrderId: data.PosOrderId,
      tableId: data.TableId,
      servantId: data.ServantId,
      servantInfo: data.ServantInfo,
      id: data.Id,
      items: Dao.parseCollection(data.OrderItems,this.parseItems),
      payments: Dao.parseCollection(data.OrderPayments,this.parsePayments),
      users: Dao.parseCollection(data.OrderUsers,this.parseOrderUsers),
      isClosed: data.IsClosed,
      currentPercentagePaid: data.CurrentPercentagePaid,
      modifiers: Dao.parseCollection(data.Modifiers,this.parseModifiers),
      orderTotals: data.OrderTotals
    };
  },
  parseItems(data) {
    return {
      id: data.Id,
      status: data.Status,
      menuItem: data.MenuItem,
      quantity: data.Quantity
    };
  },
  parsePayments(data) {
    return {
      id: data.Id,
      billType: data.BillType,
      consumption: data.Consumption,
      timestamp: new Date(data.Timestamp),
      kitchenTip: data.KitchenTip,
      percentage: data.Percentage,
      serviceTip: data.ServiceTip,
      transactionId: data.TransactionId,
      user: data.User,      //TODO: make this recursive.
      paidFor: data.PaidFor       //TODO: make this recursive
    };
  },
  parseOrderUsers(data) {
    return {
      id: data.Id,
      isNew: data.IsNew,
      role: data.Role,
      userId: data.UserId
    };
  },
  parseMenuItem(data) {
    return {
      id: data.Id,
      isRatable: data.IsRatable,
      name: data.Name,
      posItemId: data.PosItemId,
      description: data.Description,
      price: data.Price,
      extra: data.Extra
    };
  },
  parseModifiers(data) {
    return {
      id: data.Id,
      quantity: data.Quantity,
      price: data.Price
    };
  }
};

export default OrderDao;
