var SessionContext={
  clientId:'javascript',
  getClientId(){
    return this.clientId;
  },
  setClientId(clientId){
    this.clientId=clientId;
  },
  getAccessToken(){
    var token=sessionStorage.getItem('session');
    if(!token){
      return null;
    }
    else return token;
  },
  setSession(sessionInformation){
    sessionStorage.setItem('session', sessionInformation.accessToken);
  }
};

export default SessionContext;
