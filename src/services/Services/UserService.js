import config from '../config'
import AbstractServiceDao from './AbstractServiceDao'
import Dao from './Dao'

var UserDao = {
  currentService:null,
  get service(){
    if (this.currentService == null) {
      return new AbstractServiceDao(config.settings.services.userService.path);
    }
    else return this.currentService;
  },
  GetUsers(filter, successCallback, errorCallback) {
    this.service.retrieve(config.settings.services.userService.methods.crud, {filter: filter}, (responseData) => {
      successCallback(Dao.parseCollection(responseData, this.parseUser));
    }, errorCallback);
  },
  getCustomersById(Id,successCallback, errorCallback) {
    this.service.insert(config.settings.services.userService.methods.crud + "/byid", {Id: Id}, (responseData) => {
      successCallback(this.parseUserById(responseData));
    }, errorCallback);
  },
  parseUser(data) {
    return {
      id: data.Id,
      avatar: data.Avatar,
      lastName: data.LastName,
      preName: data.PreName,
      allocationId: data.AllocationId,
      allocationNumber: data.AllocationNumber,
      atTableSince: data.AtTableSince,
      unallocatedSince: data.UnallocatedSince
    };
  },
  parseUserById(data) {
    return {
      id: data.Id,
      avatar: data.Avatar,
      lastName: data.LastName,
      preName: data.PreName
    };
  },
  parseUserLogin(data) {
    return {
      avatar: data.Avatar,
      email: data.Email,
      id: data.Id,
      lastName: data.LastName,
      loginName: data.LoginName,
      preName: data.PreName,
      lastContact: data.LastContact
    };
  }
};

export default UserDao;
