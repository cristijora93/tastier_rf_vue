import Misc from '../../helpers/utilities/Misc'
import Logger from '../../helpers/utilities/Logger'
import config from '../config'
import {RequestType,LogType} from '../../helpers/constants/data/service-constants'

var version = config.settings.version;

function AbstractServiceDao(serviceUrl) {
  this.baseUrl = null;
  this.apiPath = null;
  this.baseUrl = serviceUrl;
  this.apiPath = config.settings.services.apiPath;
}

AbstractServiceDao.prototype.retrieve = function (serviceUrl, filter, finishCallback, errorCallback, headers) {
  this.doXHR(serviceUrl, RequestType.GET , filter, finishCallback, errorCallback, false, headers);
};
AbstractServiceDao.prototype.insert = function (serviceUrl, data, successCallBack, errorCallback, headers) {
  this.doXHR(serviceUrl, RequestType.POST , data, successCallBack, errorCallback, false, headers);
};
AbstractServiceDao.prototype.insertJSON = function (serviceUrl, data, successCallBack, errorCallBack, headers) {
  this.doJsonXHR(serviceUrl, RequestType.POST , data, successCallBack, errorCallBack, headers);
};

AbstractServiceDao.prototype.doXHR = function (serviceUrl, requestType, params, successCallback, errorCallback, isMultipartConfig=false, headers) {
  var _url = this.apiPath + this.baseUrl + serviceUrl;
  var requestOptions = {
    url: config.apiConfig.endpoints.activeEndpoint + _url,
    dataType: "json",
    beforeSend: (xhr) => {
      return setRequestHeaders(xhr, headers);
    },
    success(data){
      if (successCallback !== undefined) {
        successCallback.call(this, data);
      }
    },
    error(context, reason, errorThrown){
      if ((context.status === 200 || context.status === 201) && context.responseText === '') {
        if (successCallback !== undefined) {
          successCallback.call(this);
        }
        return;
      }
      Logger.log("Network error, Reason: " + reason + ", Status: " + context.statusText + " " + context.status, LogType.WARNING);
     /* if (context.status === 401) {
        if (config.globalEvents.OnSessionExpired != null)
          config.globalEvents.OnSessionExpired();
      }*/
      if (errorCallback !== undefined) {
        var parsedJson;
        try {
          parsedJson = context.responseText !== undefined ? $.parseJSON(context.responseText) : {};
        }
        catch (e) {
          parsedJson = {};
        }

        errorCallback.call(this, reason, parsedJson, context.status);
      }
    }
  };
  requestOptions=addRequestType(requestOptions,requestType);

  if (params !== undefined && params != null) {
    for (var i in params) {
      if (params[i] instanceof Date)
        params[i] = Misc.toMicrosoftDate(params[i]);
    }
    requestOptions['data'] = params;
  }
  if (requestType == RequestType.DELETE) {
    delete requestOptions['dataType'];
  }
  if (isMultipartConfig) {
    requestOptions['contentType'] = false;
    requestOptions['processData'] = false;
  }
  $.support.cors = true;
  $.ajax(requestOptions);
};
AbstractServiceDao.prototype.doJsonXHR = function (serviceUrl, requestType, params, successCallback, errorCallback, headers) {
  var _url = this.apiPath + this.baseUrl + serviceUrl;
  var requestOptionsJson = {
    url: config.apiConfig.endpoints.activeEndpoint + _url,
    dataType: "json",
    contentType: "application/json",
    beforeSend(xhr){
      return setRequestHeaders(xhr, headers);
    },
    success(data){
      if (successCallback !== undefined) {
        successCallback.call(this, data);
      }
    },
    error(context, reason,status){
      if ((context.status == 200 || context.status == 201) && context.responseText == '') {
        if (successCallback !== undefined) {
          successCallback.call(this);
        }
        return;
      }
      Logger.log("Network error, Reason: " + reason + ", Status: " + context.statusText + " " + context.status, LogType.WARNING);
     /* if (context.status == 401) {
        if (config.globalEvents.OnSessionExpired != null)
          config.globalEvents.OnSessionExpired();
      }*/
      if (errorCallback !== undefined) {
        var parsedJson;
        try {
          parsedJson = context.responseText !== undefined ? $.parseJSON(context.responseText) : {};
        }
        catch (e) {
          parsedJson = {};
        }

        errorCallback.call(this, reason, parsedJson,context.status);
      }
    }
  };
  if (params) {
    requestOptionsJson['data'] = JSON.stringify(params);
  }
  requestOptionsJson=addRequestType(requestOptionsJson,requestType);

  $.ajaxSetup({cache: false}); // prevent get calls browser-side caching. attempt to fix the ajax call failing scenario
  $.support.cors = true;
  $.ajax(requestOptionsJson);
};


function setRequestHeaders(xhr, headers) {
  xhr.setRequestHeader('version',version);
  var token = sessionStorage.getItem('session');
  if (token) {
    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
  }
  if (headers) {
    for (var i in headers) {
      xhr.setRequestHeader(i, headers[i]);
    }
  }
}
function addRequestType(requestOptionsJson,requestType){
  switch(requestType){
    case RequestType.GET:
      requestOptionsJson['type'] = 'GET';
      break;
    case RequestType.POST:
      requestOptionsJson['type'] = 'POST';
      break;
    case RequestType.PATCH:
      requestOptionsJson['type'] = 'PATCH';
      break;
    case RequestType.DELETE:
      requestOptionsJson['type'] = 'DELETE';
      break;
  }
  return requestOptionsJson;
}
export default AbstractServiceDao;
