import Logger from './Logger'
export default{
  serialzeRecursivValues(object){
    var returnableValue = {
      type: typeof object,
      data: null
    };
    if (object == null || object == undefined) {
      return returnableValue;
    }
    if (object.getMonth) {
      returnableValue['type'] = 'date';
      returnableValue['data'] = object.getTime();
    }
    else if (typeof object == 'object') {
      returnableValue['data'] = {};
      for (var key in object) {
        returnableValue['data'][key] = serialzeRecursivValues(object[key]);
      }
    }
    else {
      returnableValue['data'] = object;
    }
    return returnableValue;
  },
  serialize(object){
    return JSON.stringify(this.serialzeRecursivValues(object));
  },
  deSerializeRecursiveValues(literal){
    switch (literal.type.toLowerCase()) {
      case 'number':
        return literal.data != null ? +literal.data : null;
        break;
      case 'string':
        return literal.data != null ? "" + literal.data : null;
        break;
      case 'boolean':
        return literal.data != null ? literal.data : null;
        break;
      case 'date':
        if (literal.data == null)
          return null;
        var date = new Date();
        date.setTime(parseInt(literal.data));
        return date;
        break;
      case 'object':
        if (literal.data == null)
          return null;
        var newObject = {};
        for (var i in literal.data) {
          newObject[i] = this.deSerializeRecursiveValues(literal.data[i]);
        }
        return newObject;
        break;
      default:
        Logger.log("Can not deserialize type " + literal.type, 1 /* WARNING */);
    }
  },
  deSerialize(json){
    var storedValue = JSON.parse(json);
    return deSerializeRecursiveValues(storedValue);
  },

}
