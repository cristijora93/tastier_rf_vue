/**
 * Created by cristian.jora on 04.09.2016.
 */
import {LogType} from '../constants/data/service-constants'
export default{
  log(message, level){
    if (level === undefined)
      level = LogType.FATAL;
    switch (level) {
      case LogType.NOTIFICATION:
        window.console.log("Logger: " + message);
        break;
      case LogType.WARNING:
        window.console.warn("Logger: " + message);
        break;
      case LogType.FATAL:
        window.console.error("Logger: " + message);
        throw "exit";
        break;
    }
  }
}
