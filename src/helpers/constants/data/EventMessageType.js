export default{
  Undefined: 0,
  UserCheckIn: 1,
  UserCheckOut: 2,
  UserAllocation: 3,
  UserDeallocation: 4,
  RequestForBill: 5,
  TableClosed: 6,
  TableSplit: 7,
  TableMerge: 8,
  PaymentMissing: 9,
  UserCheckinReminder: 10,
  CloseOrder: 11,
  Disconnected: 12,
  Reconnected:13,
  OrderClosed:14
};
