/**
 * Created by cristian.jora on 18.09.2016.
 */
export default{
  Undefined: 0,
  CancelOrder: 1,
  CloseTable: 2,
  OrderClosed: 3,
  AddOrder: 4
}
