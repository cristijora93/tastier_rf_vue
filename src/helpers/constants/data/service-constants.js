var AuthenticationSuccess = {
  0: "InvalidCredentials",
  1: "Blocked",
  2: "ServiceError"
};

var AuthScope = {
  App: 'App',
  Pos: 'Pos'
};

var RequestType = {
  GET: 0,
  POST: 1,
  PATCH: 2,
  DELETE: 3,
};

var LogType = {
  NOTIFICATION: 0,
  WARNING: 1,
  FATAL: 2
};

export {AuthScope, AuthenticationSuccess, RequestType,LogType};
