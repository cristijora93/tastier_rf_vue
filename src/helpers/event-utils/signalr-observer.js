//observer for signalR functions
import PosEmulator from './PosEmulator'
import {parseTable} from '../../services/Services/PosService'
import config from '../../services/config'
var version=config.settings.version;
export default function () {
  function Observer() {
    this.handlers = [];
  }

  Observer.prototype = {
    //subscription: an object or a function which is to be subscribed.
    //   {
    //      func: the function to be subscribed
    //      priority: an int to set the order in which the functions to be called. higher number -> called faster. by default 0
    //   }
    // if priority field is missing or if subscription is a function (instead of an object), priority will be 0
    subscribe: function (subscription) {
      if (typeof subscription === 'function')
        subscription = functionToSubscriptionObject(subscription);
      if (subscription.priority == undefined || subscription.priority == null)
        subscription.priority = 0;
      var i;
      for (i = 0; i < this.handlers.length; i++)
        if (this.handlers[i].priority < subscription.priority)
          break;
      this.handlers.splice(i, 0, subscription);
    },

    unsubscribe: function (fn) {
      this.handlers = this.handlers.filter(
        function (item) {
          return fn != item.func;
        });
    },

    fire: function (arg) {
      var scope = window;
      this.handlers.forEach(function (item) {
        item.func.call(scope, arg);
      });
    }
  };

  function functionToSubscriptionObject(fn) {
    return {
      func: fn,
      priority: 0
    }
  }

  //each function from server that communicates with signalr should appear here
  var SignalrObserver = {
    getAllMessages: new Observer(),
    onMessageReceived: new Observer(),
    forget: new Observer(),
    forgetMany: new Observer(),
    onTableStatusChanged: new Observer(),
    onProfileChanged: new Observer(),
    logout: new Observer()
  };

  var hub = $.connection.posTastierPosHub;

  hub.client.getAllMessages = function (messages) {
    SignalrObserver.getAllMessages.fire(messages);
  };

  hub.client.onMessageReceived = function (message) {
    SignalrObserver.onMessageReceived.fire(message);
  };

  hub.client.forget = function (messageId) {
    SignalrObserver.forget.fire(messageId);
  };

  hub.client.forgetMany = function (messagesIds) {
    SignalrObserver.forgetMany.fire(messagesIds);
  };

  hub.client.onTableStatusChanged = function (tableId, table) {
    SignalrObserver.onTableStatusChanged.fire(parseTable(table));
  };

  hub.client.onProfileChanged = function (tableId, table) {
    SignalrObserver.onProfileChanged.fire(parseTable(table));
  };

  hub.client.logout = function(){
    SignalrObserver.logout.fire();
  };

  //signalr settings

  var emulator = new PosEmulator();
  var apiUrl = emulator.getEndpoint();

  $.connection.hub.url = apiUrl + '/signalr';
  $.connection.hub.logging = true;
  $.connection.hub.disconnected(function () {
    window.console.log("Disconnected");
    setTimeout(function () {
      $.connection.hub.start(/*{transport: ['serverSentEvents', 'longPolling']}*/)
        .done(function () {
          window.console.log('Reconnect is OK!');
        })
        .fail(function () {
          window.console.log('Could not reconnect!');
        });
    }, 0); // Restart connection after 0 seconds.
  });
  var token = sessionStorage.getItem("session");
  $.connection.hub.qs = { 'access_token' : token,'version':version };
  $.connection.hub.logging = true;

  $.connection.hub.start(/*{transport: ['serverSentEvents', 'longPolling']}*/)
    .done(function () { window.console.log("Connection hub started!") })
    .fail(function () { window.console.log('Could not connect!'); });

  return SignalrObserver;

};
