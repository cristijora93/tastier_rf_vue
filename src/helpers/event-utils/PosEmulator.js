import config from '../../services/config'
function PosEmulator() {
  var _this = this;
  this.apiEndpoint = null;
  this.apiEndpoint = config.apiConfig.endpoints.activeEndpoint;
  this.$hubSection = $('.sidebar > .hubsection');

  this.$hubSection.find('#hubConnector').click(function () {
    _this.clientHub.connect();
  });
  this.$hubSection.find('#hubDisconnector').click(function () {
    _this.clientHub.disconnect();
  });
}
PosEmulator.prototype.getEndpoint = function () {
  return this.apiEndpoint;
};

export default PosEmulator;
