//js file with necessary plugins
//Note that jquery is imported globally in webpack.base.conf.js
import bootstrap from 'bootstrap'
import jquerySignalR from '../static/js/plugins/jquery.signalR-2.2.0.js'
import signalrCore from '../static/js/plugins/signalr.core.js'
import signalrHubs from '../static/js/plugins/signalr.hubs.js'

//editors may mark these as unused although they are imported here as libraries and used globally
